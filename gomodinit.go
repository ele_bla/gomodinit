package main

import (
	"flag"
	"log"
	"os/exec"
	"strings"
)

var (
	path      = flag.String("pwd", "./", "")
	delimiter = flag.String("d", "src", "")
)

func main() {
	flag.Parse()
	log.Printf("default values-> path: %s, delimiter: %s\n", *path, *delimiter)
	pwd, err := exec.Command("pwd").Output()
	if err != nil {
		log.Println("command pwd error: ", err)
		return
	}
	*path = string(pwd)
	*path = strings.Trim(*path, "\n")

	log.Println("new path: ", *path)
	var nameModInit string = "proof"
	if strings.Contains(*path, *delimiter) {
		nameModInit = strings.Split(*path, *delimiter)[1]
		nameModInit = nameModInit[1:]
	}
	log.Println("nameModInit: ", nameModInit)
	err = exec.Command("go", "mod", "init", nameModInit).Run()
	if err != nil {
		log.Println("exec command, error: ", err)
	}
}
